//
//  DetailViewController.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var purchaseButton: UIButton!
    lazy var viewModel = DetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.getTitle
        setupTableView()
        setupPurchaseButton()
        setupFavoriteOnNavigation(isFavorite: false)
    }
    
    private func setupFavoriteOnNavigation(isFavorite: Bool) {
        guard viewModel.shouldShowFavorite else {
            self.navigationItem.rightBarButtonItem = nil
            return
        }
        let selectedImage = UIImage(named: "favoriteNav_on")?.withRenderingMode(.alwaysTemplate)
        let unselectedImage = UIImage(named: "favoriteNav_off")?.withRenderingMode(.alwaysTemplate)
        
        let image = isFavorite ? selectedImage : unselectedImage
        let favItem = UIBarButtonItem(image: image,
                                      style: .plain,
                                      target: self,
                                      action: #selector(addToFavoriteAction))
        
        self.navigationItem.rightBarButtonItem = favItem
    }
    
    @objc func addToFavoriteAction() {
        setupFavoriteOnNavigation(isFavorite: true)
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.register(ProductImageTableViewCell.self, forCellReuseIdentifier: String(describing: ProductImageTableViewCell.self))
        tableView.register(TextTableViewCell.self, forCellReuseIdentifier: String(describing: TextTableViewCell.self))
    }
    
    private func setupPurchaseButton() {
        purchaseButton.setTitleForAllStates(title: viewModel.getPurchaseTitle)
        purchaseButton.setTheme(withStyle: .purchaseButtonStyle)
    }
    
    func setObject(object: ChurrosObject) {
        viewModel.setObject(object: object)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: CELL CREATION
    
    private func createImageCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductImageTableViewCell = ProductImageTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        if let image = viewModel.getProductImage() {
            cell.setImage(image: image)
        }
        return cell
    }
    
    private func createTextCell(tableView: UITableView, indexPath: IndexPath, type: ProductDetailCellType) -> UITableViewCell {
        let cell: TextTableViewCell = TextTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        let text = viewModel.getText(fromCellType: type)
        cell.fill(dto: text)
        return cell
    }
    
    // MARK: PURCHASE ACTION

    @IBAction func purchaseAction(_ sender: Any) {
        let alertBuilder = AlertBuilder(fromController: self.navigationController, dataSource: viewModel.getAddedToCartMessage())
        
        DispatchQueue.main.async {
            alertBuilder.buildAndPresent()
        }
    }
}
extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = viewModel.getCellType(forIndex: indexPath) else {
            return UITableViewCell()
        }
        switch cellType {
        case .productImage:
            return createImageCell(tableView: tableView, indexPath: indexPath)
        case .title, .price, .description, .promotion:
            return createTextCell(tableView: tableView, indexPath: indexPath, type: cellType)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getCellHeight(atIndex: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getCellHeight(atIndex: indexPath)
    }
    
}
