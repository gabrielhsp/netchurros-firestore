//
//  DetailViewModel.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

enum ProductDetailCellType {
    case productImage
    case title
    case price
    case promotion
    case description
    
    var cellHeight: CGFloat {
        switch self {
        case .productImage, .description, .title:
            return UITableView.automaticDimension
        case .price:
            return 40
        case .promotion:
            return 44
        }
    }
}

class DetailViewModel {
    
    private var churro: ChurrosObject? {
        didSet {
            populateCells()
        }
    }
    
    private var cellTypes: [ProductDetailCellType]?
    
    func setObject(object: ChurrosObject) {
        self.churro = object
    }
    
    init() {
        
    }
    
    // MARK: CELL SETUP
    
    func numberOfRows() -> Int {
        return cellTypes?.count ?? 0
    }
    
    func getCellType(forIndex index: IndexPath) -> ProductDetailCellType? {
        guard let cellType = cellTypes else {
            return nil
        }
        return cellType[index.row]
    }
    
    func getCellHeight(atIndex index: IndexPath) -> CGFloat {
        return getCellType(forIndex: index)?.cellHeight ?? .leastNonzeroMagnitude
    }
    
    private func populateCells() {
        var cells = [ProductDetailCellType]()
        
        cells.append(.productImage)
        cells.append(.title)
        cells.append(.price)
        
        if shouldShowPromotion {
            cells.append(.promotion)
        }
        if shouldShowDescription {
            cells.append(.description)
        }
        cellTypes = cells
    }
    
    var getTitle: String {
        return churro?.title ?? ""
    }
    
    var getPurchaseTitle: String {
        return "COMPRAR"
    }
    
    // MARK: CELLS DTO SETUP
    
    func getProductImage() -> UIImage? {
        guard let churro = churro, !churro.image.isEmpty else {
            return nil
        }
        return UIImage(named: churro.image)
    }
    
    func getText(fromCellType type: ProductDetailCellType) -> TextCellDto {
        switch type {
        case .title:
            return TextCellDto(text: churro?.title ?? "", theme: .titleLabelStyle)
        case .description:
            return TextCellDto(text: churro?.description ?? "", theme: .descriptionLabelStyle)
        case .price:
            return TextCellDto(text: churro?.price.currencyValue ?? "", theme: .priceLabelStyle)
        case .promotion:
            let promotionTitle = getPromotionTitle()
            return TextCellDto(text: promotionTitle, theme: .promotionLabelStyle)
        default:
            return TextCellDto(text: "", theme: .titleLabelStyle)
        }
    }
    
    private func getPromotionTitle() -> String {
        let defaultPromotionValue = "FRETE GRATIS"
        return FirebaseRemoteConfigLoader.sharedInstance.getStringValue(fromKey: .promotionName) ?? defaultPromotionValue
    }
    
    // MARK: TOGGLE VERIFICATIONS
    
    var shouldShowPromotion: Bool {
        return shouldShowFeature(withKey: .promotionEnabled)
    }
    
    var shouldShowDescription: Bool {
        return shouldShowFeature(withKey: .descriptionEnabled)
    }
    
    var shouldShowFavorite: Bool {
        return shouldShowFeature(withKey: .favoriteIsEnabled)
    }
    
    private func shouldShowFeature(withKey key: ToggleKey) -> Bool {
        return ToggleRequestManager.toggleIsEnabled(forKey: key) ?? false
    }
    
    // MARK: ALERT SETUP
    
    func getAddedToCartMessage() -> (title: String, message: String) {
        return (title: "Sucesso!", message: "Produto adicionado ao carrinho.")
    }
    
}
