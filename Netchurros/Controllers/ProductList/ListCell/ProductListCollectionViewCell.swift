//
//  ProductListCollectionViewCell.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 24/08/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct ProductListDTO {
    var title = ""
    var imageNamed = ""
    var price = ""
    var shouldShowFavorite = false
}

final class ProductListCollectionViewCell: UICollectionViewCell {
    
    lazy var productImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 14
        imageView.contentMode = .scaleToFill
        imageView.clearsContextBeforeDrawing = true
        imageView.autoresizesSubviews = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        let unselectedImage = UIImage(named: "favoriteNav_off")?.withRenderingMode(.alwaysTemplate)
        button.contentMode = .scaleAspectFit
        // TODO: INSERT THEMECOLOR
        button.tintColor = .white
        button.setImage(unselectedImage, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var productTitle: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        return label
    }()
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setTheme()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: LAYOUT SETUP
    
    private func setTheme() {
        productTitle.setTheme(withStyle: .titleLabelStyle)
        priceLabel.setTheme(withStyle: .priceLabelStyle)
    }
    
    private func setupView() {
        addSubview(productTitle)
        addSubview(productImageView)
        addSubview(favoriteButton)
        addSubview(priceLabel)
        bringSubviewToFront(favoriteButton)
    }
    
    private func setupConstraints()  {
        let marginConstant: CGFloat = 10
        NSLayoutConstraint.activate([
            productImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: marginConstant),
            productImageView.topAnchor.constraint(equalTo: topAnchor, constant: marginConstant),
            productImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -marginConstant),
            
            priceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            favoriteButton.topAnchor.constraint(equalTo: productImageView.topAnchor, constant: 3.5),
            favoriteButton.trailingAnchor.constraint(equalTo: productImageView.trailingAnchor, constant: -4),
            favoriteButton.widthAnchor.constraint(equalToConstant: 42),
            favoriteButton.heightAnchor.constraint(equalToConstant: 42),
            
            productImageView.heightAnchor.constraint(equalTo: productImageView.widthAnchor, multiplier: (1.0 / 1.0)),
            productImageView.bottomAnchor.constraint(equalTo: productTitle.topAnchor, constant: -4),
            
            productTitle.topAnchor.constraint(equalTo: productImageView.bottomAnchor, constant: 4),
            productTitle.leadingAnchor.constraint(equalTo: productImageView.leadingAnchor),
            productTitle.trailingAnchor.constraint(equalTo: productImageView.trailingAnchor),
            
            priceLabel.leadingAnchor.constraint(equalTo: productImageView.leadingAnchor),
            priceLabel.trailingAnchor.constraint(equalTo: productImageView.trailingAnchor),
            priceLabel.topAnchor.constraint(equalTo: productTitle.bottomAnchor, constant: 8)
        ])
    }
    
    // MARK: FAVORITE ACTION
    
    @objc private func favoriteAction() {
        let selectedImage = UIImage(named: "favoriteNav_on")?.withRenderingMode(.alwaysTemplate)
        selectAndAnimate(button: favoriteButton, image: selectedImage)
    }
    
    private func selectAndAnimate(button: UIButton, image: UIImage?) {
        button.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: CGFloat(0.20), initialSpringVelocity: CGFloat(6.0), options: .curveEaseInOut, animations: {
            button.setImage(image, for: .normal)
            button.transform = .identity
        })
    }
    
    // MARK: FILL
    
    func fill(dto: ProductListDTO) {
        productTitle.text = dto.title
        priceLabel.text = dto.price
        productImageView.image = UIImage(named: dto.imageNamed)
        favoriteButton.isHidden = !dto.shouldShowFavorite
    }
    
}
