//
//  ListViewModel+extension.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 24/08/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

extension ListViewModel {
    
    func fetchChurros() -> [ChurrosObject] {
        let currentChurros: [ChurrosObject] = [
            ChurrosObject(title: "Churros Sortidos mais gostosos que boleto pago", sku: "D23-444", price: 1299, image: "sorted", description: "Este churros definitivamente é bom, ou não, vai que você não gosta também. Ninguém é obrigado a nada.", sizes: ["P", "M", "G"], toppings: [Toppings(name: "CHOCOLATE", id: 1)]),
            ChurrosObject(title: "Bolo de churros", sku: "D23-445", price: 2300, image: "cake", description: "Este bolo definitivamente é um bolo... com churros... não coma se você é diabético.", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1)]),
            ChurrosObject(title: "Copo de churros", sku: "D23-455", price: 1000, image: "cup", description: "Churros, só que num copo.", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "BAUNILHA", id: 3)]),
            ChurrosObject(title: "Pão de churros", sku: "D23-475", price: 999999999, image: "bread", description: "Pão de Churros, nem sei se é bom. Acharia melhor fazer só churros mesmo. Imagina o trabalho?", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "BAUNILHA", id: 3)]),
            ChurrosObject(title: "Cochurros", sku: "D23-488", price: 6000, image: "cochurros", description: "Cochurros: Porque o brasileiro foi longe demais.", sizes: ["P", "M", "G", "GG"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Mini-churros", sku: "D23-888", price: 3500, image: "mini-churros", description: "Neeeeiva, é só o sinarzin de churros!", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Churros Simples", sku: "D23-858", price: 4900, image: "detailed", description: "Churros ué...", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Churros Sortidos mais gostosos que boleto pago", sku: "D23-444", price: 1299, image: "sorted", description: "Este churros definitivamente é bom, ou não, vai que você não gosta também. Ninguém é obrigado a nada.", sizes: ["P", "M", "G"], toppings: [Toppings(name: "CHOCOLATE", id: 1)]),
            ChurrosObject(title: "Bolo de churros", sku: "D23-445", price: 2300, image: "cake", description: "Este bolo definitivamente é um bolo... com churros... não coma se você é diabético.", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1)]),
            ChurrosObject(title: "Copo de churros", sku: "D23-455", price: 1000, image: "cup", description: "Churros, só que num copo.", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "BAUNILHA", id: 3)]),
            ChurrosObject(title: "Pão de churros", sku: "D23-475", price: 999999999, image: "bread", description: "Pão de Churros, nem sei se é bom. Acharia melhor fazer só churros mesmo. Imagina o trabalho?", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "BAUNILHA", id: 3)]),
            ChurrosObject(title: "Cochurros", sku: "D23-488", price: 6000, image: "cochurros", description: "Cochurros: Porque o brasileiro foi longe demais.", sizes: ["P", "M", "G", "GG"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Mini-churros", sku: "D23-888", price: 3500, image: "mini-churros", description: "Neeeeiva, é só o sinarzin de churros!", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Churros Simples", sku: "D23-858", price: 4900, image: "detailed", description: "Churros ué...", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Churros Sortidos mais gostosos que boleto pago", sku: "D23-444", price: 1299, image: "sorted", description: "Este churros definitivamente é bom, ou não, vai que você não gosta também. Ninguém é obrigado a nada.", sizes: ["P", "M", "G"], toppings: [Toppings(name: "CHOCOLATE", id: 1)]),
            ChurrosObject(title: "Bolo de churros", sku: "D23-445", price: 2300, image: "cake", description: "Este bolo definitivamente é um bolo... com churros... não coma se você é diabético.", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1)]),
            ChurrosObject(title: "Copo de churros", sku: "D23-455", price: 1000, image: "cup", description: "Churros, só que num copo.", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "BAUNILHA", id: 3)]),
            ChurrosObject(title: "Pão de churros", sku: "D23-475", price: 999999999, image: "bread", description: "Pão de Churros, nem sei se é bom. Acharia melhor fazer só churros mesmo. Imagina o trabalho?", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "BAUNILHA", id: 3)]),
            ChurrosObject(title: "Cochurros", sku: "D23-488", price: 6000, image: "cochurros", description: "Cochurros: Porque o brasileiro foi longe demais.", sizes: ["P", "M", "G", "GG"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Mini-churros", sku: "D23-888", price: 3500, image: "mini-churros", description: "Neeeeiva, é só o sinarzin de churros!", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Churros Simples", sku: "D23-858", price: 4900, image: "detailed", description: "Churros ué...", sizes: ["UNICO"], toppings: [Toppings(name: "DOCE DE LEITE", id: 1), Toppings(name: "CHOCOLATE", id: 2), Toppings(name: "CHOCOLATE AMARGO", id: 3)]),
            ChurrosObject(title: "Churros Sortidos mais gostosos que boleto pago", sku: "D23-444", price: 1299, image: "sorted", description: "Este churros definitivamente é bom, ou não, vai que você não gosta também. Ninguém é obrigado a nada.", sizes: ["P", "M", "G"], toppings: [Toppings(name: "CHOCOLATE", id: 1)])
        ]
        return currentChurros
    }
}
