//
//  Coordinator.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol CoordinatorRouter {
    associatedtype Controller: UIViewController
    associatedtype DataSource
    var storyBoardName: String { get set }
    init(fromController controller: UINavigationController?, dataSource: DataSource)
    func buildAndPresent()
}

class AlertBuilder: CoordinatorRouter {
    
    typealias Controller = UIAlertController
    
    typealias DataSource = (title: String, message: String)
    
    var storyBoardName: String = ""
    private var originController: UIViewController?
    private var dataSource = (title: "", message: "")
    
    required init(fromController controller: UINavigationController?, dataSource: (title: String, message: String)) {
        originController = controller
        self.dataSource = dataSource
    }
    
    func buildAndPresent() {
        let alertController = Controller(title: dataSource.title, message: dataSource.message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        originController?.present(alertController, animated: true, completion: nil)
    }
}

class DetailControllerBuilder: CoordinatorRouter {
    
    typealias Controller = DetailViewController

    typealias DataSource = ChurrosObject
    
    var storyBoardName: String = "Main"
    private var churro = ChurrosObject()
    private weak var originController: UINavigationController?
    
    required init(fromController controller: UINavigationController?, dataSource: ChurrosObject) {
        originController = controller
        churro = dataSource
    }
    
    func buildAndPresent() {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        let identifier = String(describing: Controller.self)
        guard let detailController = storyboard.instantiateViewController(withIdentifier: identifier) as? DetailViewController else {
            return
        }
        detailController.setObject(object: churro)
        originController?.pushViewController(detailController, animated: true)
    }

}
