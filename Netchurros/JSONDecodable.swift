//
//  JSONDecodable.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol JSONDecodable {
    func decodeJson<T: Codable>(fromData data: Data, completion: @escaping (_ value: T?, _ error: Error?) -> Void)
}

extension JSONDecodable {
    func decodeJson<T: Codable>(fromData data: Data, completion: @escaping (_ value: T?, _ error: Error?) -> Void) {
        do {
            let object = try JSONDecoder().decode(T.self, from: data)
            completion(object, nil)
        } catch {
            print("ERROR DECODING \(T.self)", error)
            completion(nil, error)
        }
    }
}
