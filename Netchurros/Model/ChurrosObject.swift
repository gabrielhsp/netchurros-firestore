//
//  ChurrosObject.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 24/08/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct ChurrosObject {
    
    var title = ""
    var sku = ""
    var price = 0
    var image = ""
    var description = ""
    var sizes = [String]()
    var toppings = [Toppings]()
    
}

struct Toppings {
    var name = ""
    var id = 0
}

