//
//  ThemeButtonStyleDTO.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct ThemeButtonStyleDTO {
    var enableColor: UIColor = .clear
    var disableColor: UIColor = .clear
    var borderColor: UIColor = .clear
    var borderRadius: CGFloat = 0
    var borderWidth: CGFloat = 0
    var textColor: UIColor = .clear
    var textFont: UIFont = UIFont()
    
}
