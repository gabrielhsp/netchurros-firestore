//
//  ThemeLabelStyleDTO.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct ThemeLabelStyleDTO {
    var textColor: UIColor = .clear
    var backgroundColor: UIColor = .clear
    var borderColor: UIColor = .clear
    var borderRadius: CGFloat = 0
    var borderWidth: CGFloat = 0
    var textFont: UIFont = UIFont()
}
