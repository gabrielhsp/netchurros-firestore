//
//  ThemeManager.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct ThemeManager {
    
    private static var themes: Theme?
    
    static func fetchTheme() {
        FirebaseRemoteConfigLoader.sharedInstance.getJsonResponse(fromKey: .themes, type: Theme.self) { (themeObject, error) in
            self.themes = themeObject
        }
    }
    
    private static var navigationBarStyle: ThemeStyles? {
        return ThemeManager.getStyleWithKey(key: .navigationBarStyle)
    }
    
    private static func labelStyle(withKey key: ThemeKeys) -> ThemeStyles? {
        return ThemeManager.getStyleWithKey(key: key)
    }
    
    private static func buttonStyles(withKey key: ThemeKeys) -> ThemeStyles? {
        return ThemeManager.getStyleWithKey(key: key)
    }
    
    // MARK: NAVIGATION BAR
    
    static func getThemeForNavigationBar() -> (barColor: UIColor, textColor: UIColor)? {
        guard let themeBar = navigationBarStyle, let navBarColor = themeBar.styleProperties.first(where: { $0.key == ThemeProperty.barColor.rawValue }), let navTextColor = themeBar.styleProperties.first(where: { $0.key == ThemeProperty.textColor.rawValue }) else {
            return nil
        }
        
        let barColor = ThemeManager.getColorFor(colorStyle: ThemeColor(rawValue: navBarColor.value))
        let textColor = ThemeManager.getColorFor(colorStyle: ThemeColor(rawValue: navTextColor.value))
        return (barColor, textColor)
    }
    
    // MARK: Button
    
    static func getThemeForButton(withStyle style: ThemeKeys) -> ThemeButtonStyleDTO? {
        guard let properties = ThemeManager.buttonStyles(withKey: style)?.styleProperties else {
            return nil
        }
        return ThemeButtonStyleDTO(enableColor: getColor(properties: properties, key: .enableColor),
                                   disableColor: getColor(properties: properties, key: .disableColor),
                                   borderColor: getColor(properties: properties, key: .borderColor),
                                   borderRadius: getCGFloatSize(properties: properties, key: .borderRadius),
                                   borderWidth: getCGFloatSize(properties: properties, key: .borderWidth),
                                   textColor: getColor(properties: properties, key: .textColor),
                                   textFont: getUIFont(properties: properties, key: .borderWidth))
    }
    
    // MARK: Label
    
    static func getThemeForLabel(withStyle keyStyle: ThemeKeys) -> ThemeLabelStyleDTO? {
        guard let properties = labelStyle(withKey: keyStyle)?.styleProperties else {
            return nil
        }
        return ThemeLabelStyleDTO(textColor: getColor(properties: properties, key: .textColor),
                                  backgroundColor: getColor(properties: properties, key: .backgroundColor),
                                  borderColor: getColor(properties: properties, key: .borderColor),
                                  borderRadius: getCGFloatSize(properties: properties, key: .borderRadius),
                                  borderWidth: getCGFloatSize(properties: properties, key: .borderWidth),
                                  textFont: getUIFont(properties: properties, key: .borderWidth))
    }
    
    private static func getUIFont(properties: [StyleProperties], key: ThemeProperty) -> UIFont {
        let size = getCGFloatSize(properties: properties, key: .textFontSize)
        let style = getTextFontStyle(properties: properties, key: .textFontStyle)
        return style.textFont(size: size)
    }
    
    private static func getTextFontStyle(properties: [StyleProperties], key: ThemeProperty) -> TextFontStyle {
        guard let style = properties.first(where: { $0.key == key.rawValue })?.value else {
            return .normal
        }
        return TextFontStyle(rawValue: style)
    }
    
    private static func getCGFloatSize(properties: [StyleProperties], key: ThemeProperty) -> CGFloat {
        guard let size = properties.first(where: { $0.key == key.rawValue })?.value else {
            return 0
        }
        return CGFloat(Int(size) ?? 0)
    }
    
    private static func getColor(properties: [StyleProperties], key: ThemeProperty) -> UIColor {
        guard let color = properties.first(where: { $0.key == key.rawValue })?.value else {
            return .clear
        }
        return ThemeManager.getColorFor(colorStyle: ThemeColor(rawValue: color))
    }
    
    private static func getStyleWithKey(key: ThemeKeys) -> ThemeStyles? {
        return themes?.styles.ios.first(where: { $0.styleName == key.rawValue })
    }
    
    static func getColorFor(colorStyle: ThemeColor?, alpha: CGFloat = 1.0) -> UIColor {
        guard let colorStyle = colorStyle, let color = themes?.styles.colorProperties.first(where: { $0.key == colorStyle.rawValue }) else {
            return UIColor(hexString: "#222222", alpha: alpha)
        }
        return UIColor(hexString: color.value, alpha: alpha)
    }
    
}
