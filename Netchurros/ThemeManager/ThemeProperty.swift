//
//  ThemeProperty.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum ThemeProperty: String {
    
    case barColor
    case textColor
    case textFontSize
    case textFontStyle
    case borderColor
    case borderRadius
    case borderWidth
    case enableColor
    case disableColor
    case highlightedColor
    case iconURL
    case backgroundColor
    case alpha
    case placeholderColor
    case placeholderAlpha
    case tintColor
}
