//
//  Foundation+extensions.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

extension String {
    func indexGiven(character: Character) -> Int? {
        guard let index = firstIndex(of: character) else {
            return nil
        }
        return distance(from: startIndex, to: index)
    }
}

extension Int {
    var currencyValue: String {
        return Float(self).currencyValue
    }
}
extension Float {
    
    private var currencyCharacter: Character {
        let character: Character = "$"
        return character
    }
    
    var currencyValue: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        let language = "pt_BR"
        formatter.locale = Locale(identifier: language)
        var currency = formatter.string(from: NSNumber(value: self > 0 ? (self / 100) : self )) ?? ""
        guard let index = currency.indexGiven(character: currencyCharacter), currency.count >= index + 1 else {
            return currency
        }
        if !currency.contains(" ") {
            currency.insert(" ", at: currency.index(currency.startIndex, offsetBy: +(index + 1)))
        }
        return currency
    }
}
