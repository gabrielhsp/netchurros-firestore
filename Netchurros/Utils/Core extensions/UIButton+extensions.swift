//
//  UIButton+extensions.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UIButton {
    private var allStates: [UIControl.State] {
        return [.normal, .highlighted, .selected, .disabled]
    }
    
    func setTitleForAllStates(title: String) {
        allStates.forEach({ setTitle(title, for: $0) })
    }
    
    func setBackgroundColorForState(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let graphic = UIGraphicsGetCurrentContext() {
            graphic.setFillColor(color.cgColor)
            graphic.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        }
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        setBackgroundImage(colorImage, for: forState)
    }
    
    func setTheme(withStyle style: ThemeKeys) {
        guard let dto = ThemeManager.getThemeForButton(withStyle: style) else {
            return
        }
        allStates.forEach { setBackgroundColorForState(color: dto.enableColor, forState: $0) }
        setBackgroundColorForState(color: dto.disableColor, forState: .disabled)
        
        layer.cornerRadius = dto.borderRadius
        layer.borderWidth = dto.borderWidth
        layer.borderColor = dto.borderColor.cgColor
        layer.masksToBounds = true
        
        guard let title = title(for: .normal) else {
            return
        }
        
        let attributes = NSAttributedString(string: title,
                                            attributes: [NSAttributedString.Key.foregroundColor: dto.textColor,
                                                         NSAttributedString.Key.font: dto.textFont])
        setAttributedTitle(attributes, for: .normal)
    }
    
}
