//
//  UITableView+extensions.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class func createCell<T: UITableViewCell>(tableView: UITableView, indexPath: IndexPath) -> T {
        return tableView.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}

